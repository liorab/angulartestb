import { Component, OnInit } from '@angular/core';
import { TodosService } from '../todos.service';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService,public todosService:TodosService) { }

  title:string;
  userId:string;

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId=user.uid;
      })
  }

  onSubmit(){
    
      this.postsService.addTodo(this.userId,this.title);
    
    this.router.navigate(['/todos']);
  }


}
