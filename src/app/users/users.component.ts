import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from '../interfaces/users';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { TodosService } from '../todos.service';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  Users$:Observable<Users>;
  userId:string;

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService,public todosService:TodosService) { }


  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId=user.uid;
      }
    )
 
    this.Users$=this.postsService.getUsers();        
  }
  saveUser(id:number,name:string,email:string){
    console.log("אבוטבול");
    this.postsService.saveUser(id,name,email);
  }

}
