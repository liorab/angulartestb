import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage'; 

//Material
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { WellcomeComponent } from './wellcome/wellcome.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { PostsComponent } from './posts/posts.component';
import { TodosComponent } from './todos/todos.component';
import { TempComponent } from './temp/temp.component';
import { SavedTodoComponent } from './saved-todo/saved-todo.component';
import { UsersComponent } from './users/users.component';
import { RegosteredUsersComponent } from './regostered-users/regostered-users.component';
import { AddTodoComponent } from './add-todo/add-todo.component';



const appRoutes: Routes = [
  { path: 'todos', component: TodosComponent },
  { path: 'wellcome', component: WellcomeComponent },
  //{ path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  
  
  //{ path: 'classified', component: ClassifiedComponent },
 // { path: 'docform', component: DocformComponent },
 
//  { path: 'posts', component: PostsComponent },
  { path: 'SavedTodos', component: SavedTodoComponent },
  { path: 'users', component: UsersComponent },
  { path: 'addTodo', component: AddTodoComponent },
  { path: 'RegosteredUsers', component: RegosteredUsersComponent },
  { path: "",
    redirectTo: '/wellcome',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    SignupComponent,
    WellcomeComponent,
    ClassifiedComponent,
    DocformComponent,
    NotfoundComponent,
    PostsComponent,
    TodosComponent,
    TempComponent,
    SavedTodoComponent,
    UsersComponent,
    RegosteredUsersComponent,
    AddTodoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    MatCheckboxModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
   providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
