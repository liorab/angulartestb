import { Component, OnInit } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { Todos } from '../interfaces/todos';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Posts } from '../interfaces/posts';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }

  Posts$:Observable<Posts[]>;
  userId:string;


  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId=user.uid;
      }
    )
  //return  this.Posts$=this.postsService.getposts();
  }

}
