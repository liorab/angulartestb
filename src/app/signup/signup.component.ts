import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService:AuthService, public router:Router, public route:ActivatedRoute) { }

  email:string;
  password:string;

  ngOnInit() {
  }
  onSubmit(){
    this.authService.signup(this.email,this.password);
  }

}
