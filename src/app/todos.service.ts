import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore/firestore';
import { Router } from '@angular/router';
import { Users } from './interfaces/users';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  usersUrl="https://jsonplaceholder.typicode.com/users";
  constructor(private http:HttpClient,private db:AngularFirestore,public router:Router,public afAuth:AngularFireAuth) { }
  password:string;
  userCollection:AngularFirestoreCollection=this.db.collection('users');
 
  getUsers(){
    
    return this.http.get<Users>(this.usersUrl)
  }
  
}
