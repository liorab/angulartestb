import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Todos } from './interfaces/todos';
import { Posts } from './interfaces/posts';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { map } from 'rxjs/operators';
import { Users } from './interfaces/users';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  public postApi="https://jsonplaceholder.typicode.com/posts/";
  public todosapi="https://jsonplaceholder.typicode.com/todos/";
  usersUrl="https://jsonplaceholder.typicode.com/users";

  constructor(private http:HttpClient,private db:AngularFirestore,public router:Router,public afAuth:AngularFireAuth, public authService:AuthService) { }

  postCollection:AngularFirestoreCollection=this.db.collection('posts');
  userCollection:AngularFirestoreCollection=this.db.collection('users');
 

  todoCollection:AngularFirestoreCollection=this.db.collection('todos');
  password:string;
  
  gettodos(){
    return this.http.get<Todos>(this.todosapi)
  }
  getUsers(){
    
    return this.http.get<Users>(this.usersUrl)
  }
  /*getposts(){
    return this.http.get<Posts[]>(this.postApi);
  }*/
 

  saveTodo(userId:string,title:string,completed:string){
    this.todoCollection=this.db.collection(`users/${userId}/todos`);
    const todo={title:title,completed:completed};
    this.userCollection.doc(userId).collection('todos').add(todo);
  }
  getSavedPTodos(userId:string){
    this.postCollection= this.db.collection(`users/${userId}/todos`);
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id= document.payload.doc.id;
            return data;
          }
        )
      )
    )
  }
  updareCheckbox(userId:string, id:string, completed:string){
   
    this.postCollection= this.db.collection(`users/${userId}/todos`);

     if(completed=='true'){
      console.log('ליאור');
      return this.db.doc(`users/${userId}/todos/${id}`).update({
      completed:"false"
    })
      
  } 
  else{
    return this.db.doc(`users/${userId}/todos/${id}`).update({
      completed:"true"
  } )  
}
  }
  saveUser(id:number,name:string,email:string){
  
    this.password='12345678';
    const user={uid:id,email:email,name:name};

    this.authService.addUser(id,name,email);

    
 
   return this.userCollection.add(user);
    
  }

  getRegisteredUsers(){
    this.postCollection= this.db.collection(`users`);
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id= document.payload.doc.id;
            return data;
          }
        )
      )
    )
  }
  addTodo(userId:string,title:string){
    const todo = {title:title, done:false};
    this.userCollection.doc(userId).collection('todos').add(todo);
  }
  updateStatus(userId:string,id:string,title:string){
    return this.db.doc(`users/${userId}/todos/${id}`).update({
      done:true
  })
}
}