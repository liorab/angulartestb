import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-regostered-users',
  templateUrl: './regostered-users.component.html',
  styleUrls: ['./regostered-users.component.css']
})
export class RegosteredUsersComponent implements OnInit {

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }

  RegisteredUsers$:Observable<any>;

  ngOnInit() {
    this.RegisteredUsers$ = this.postsService.getRegisteredUsers();
  }

}
