import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegosteredUsersComponent } from './regostered-users.component';

describe('RegosteredUsersComponent', () => {
  let component: RegosteredUsersComponent;
  let fixture: ComponentFixture<RegosteredUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegosteredUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegosteredUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
