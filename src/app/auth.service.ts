import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Users } from './interfaces/users';
import * as firebase from 'firebase';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>
  password="12345678";

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
      this.user=this.afAuth.authState;
     }

     getUser(){
       return this.user;
     }

signup(email:string,password:string){
  this.afAuth.auth.createUserWithEmailAndPassword(email,password)
  .then(()=>{
    this.router.navigate(['/wellcome']);
  }).catch((error) => {
    window.alert(error.message)
  })    
} 

login(email:string,password:string){
  return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
            this.router.navigate(['/wellcome']);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
Logout(){
  this.afAuth.auth.signOut().then(res=> console.log('successful logout'));
}

addUser(id:number,name:string,email:string){
  var config = {apiKey: "AIzaSyCiNjQJvTSYTG7HZwK9Lb7k9_RDR9dS6Ls",
    authDomain: "angulartestb.firebaseapp.com",
    databaseURL: "https://angulartestb.firebaseio.com"};
    
    var secondaryApp = firebase.initializeApp(config, "Secondary"+id);
    
    secondaryApp.auth().createUserWithEmailAndPassword(email, this.password).then(function(firebaseUser) {
        console.log("User " + firebaseUser.user.uid + " created successfully!");
      
        secondaryApp.auth().signOut();
    });

}


}
