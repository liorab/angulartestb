import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Todos } from '../interfaces/todos';
import { Users } from '../interfaces/users';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService,public todosService:TodosService) { }

  SavedTodos$:Observable<any>;
  Users$:Observable<Users>;
  userId:string;
  uid:string;
  


  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.SavedTodos$ = this.postsService.getSavedPTodos(this.userId);
       }
       
    )
     
   
  //this.todos$=this.postsService.getTodos(this.userId);  
    this.Users$=this.postsService.getUsers();        
  }

  /*savetodo(id:number,title:string,completed:string){
    return this.postsService.saveTodo(this.userId,title,completed);
  }*/

  updateStatus(id:string,title:string){
    return this.postsService.updateStatus(this.userId,id,title);
  }
}
