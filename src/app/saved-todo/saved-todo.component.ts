import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Todos } from '../interfaces/todos';
import { Observable } from 'rxjs';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-saved-todo',
  templateUrl: './saved-todo.component.html',
  styleUrls: ['./saved-todo.component.css']
})
export class SavedTodoComponent implements OnInit {

  constructor(public postsService:PostsService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }

  SavedTodos$:Observable<any>;
  userId:string;

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.SavedTodos$ = this.postsService.getSavedPTodos(this.userId);
      }
    )
  }

  updateCheckbox(id:string,completed:string){
   
    return this.postsService.updareCheckbox(this.userId,id,completed);
  }
}


