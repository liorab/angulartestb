import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedTodoComponent } from './saved-todo.component';

describe('SavedTodoComponent', () => {
  let component: SavedTodoComponent;
  let fixture: ComponentFixture<SavedTodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedTodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
