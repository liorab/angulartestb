// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCiNjQJvTSYTG7HZwK9Lb7k9_RDR9dS6Ls",
    authDomain: "angulartestb.firebaseapp.com",
    databaseURL: "https://angulartestb.firebaseio.com",
    projectId: "angulartestb",
    storageBucket: "angulartestb.appspot.com",
    messagingSenderId: "286428115552",
    appId: "1:286428115552:web:5bbc6553ec18f2060b4905",
    measurementId: "G-4WVZNZVE96"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
